#[macro_use]
extern crate structopt;
extern crate env_logger;
extern crate failure;
extern crate serialport;

use serialport::prelude::*;
use std::thread;
use std::time::Duration;
use structopt::StructOpt;

type Result<T> = std::result::Result<T, failure::Error>;

#[derive(StructOpt, Debug)]
struct Arguments {
    #[structopt(name = "serialport")]
    serial_port: String,
}

fn run(args: Arguments) -> Result<()> {
    println!("arguments: {:?}", args);

    let s = SerialPortSettings {
        //baud_rate: BaudRate::Baud1500000,
        //baud_rate: BaudRate::BaudOther(1562500),
        baud_rate: 1562500u32,
        data_bits: DataBits::Eight,
        flow_control: FlowControl::None,
        parity: Parity::None,
        stop_bits: StopBits::One,
        timeout: Duration::from_millis(1000),
    };

    let mut port = serialport::open_with_settings(&args.serial_port, &s)?;

    // Start the test
    port.write("R$".as_bytes())?;
    for _ in 0..100 {
        // Get the values
        port.write("A$".as_bytes())?;
        port.flush()?;
        // Get the status
        port.write("S$".as_bytes())?;
        port.flush()?;
        thread::sleep(Duration::from_millis(100));
    }
    // Stop the test
    port.write("H$".as_bytes())?;
    port.flush()?;

    thread::sleep(Duration::from_millis(1000));

    // Return to start position
    port.write("E$".as_bytes())?;
    port.flush()?;

    loop {
        let mut buf = vec![0u8; 1];
        port.read_exact(&mut buf)?;
        let b = buf[0];
        let c = char::from(buf[0]);
        println!("Read {} ({:?})", b, c);
    }

    Ok(())
}

fn main() {
    let mut builder = env_logger::Builder::new();
    builder.parse("griptensilelog=info");
    if let Ok(rust_log) = std::env::var("RUST_LOG") {
        builder.parse(&rust_log);
    }
    builder.init();

    if let Err(err) = run(Arguments::from_args()) {
        eprintln!("{}", err);
        std::process::exit(1);
    }
}
